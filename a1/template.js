const answerStyleA = 'color:#F56EB3;font-weight:semi-bold;font-family:Franklin Gothic;';
const answerStyleB = 'color:#93BFCF;font-weight:semi-bold;font-family:Franklin Gothic;';
const answerStyleC = 'color:none;';
console.log("%c'Javascript - Array Manipulation'\n\n%cWhy was the JavaScript developer sad? \n>> %cBecause he didn't know how to 'null' his feelings.", answerStyleB, answerStyleA, answerStyleB);
console.log('');


/*
Create functions which can manipulate our arrays.
*/

let registeredUsers = [
    
    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*

1. Create a function which will allow us to register into the registeredUsers list.
- this function should be able to receive a string.
- determine if the input username already exists in our registeredUsers array.
-if it is, show an alert window with the following message:
"Registration failed. Username already exists!"
-if it is not, add the new username into the registeredUsers array and show an alert:
"Thank you for registering!"
- invoke and register a new user.
- outside the function log the registeredUsers array.

*/
function register(user) {

    if (user === undefined || user === null) {
        return;
    } else if (registeredUsers.includes(user)) {
        alert(`Registration failed. ${user} already exists!`);
        console.log(`%c${user} %calready exists!%c\n>> %cregisteredUsers array:`, answerStyleA, answerStyleC, answerStyleA, answerStyleC, registeredUsers);
    } else {
        registeredUsers.push(user);
        alert(`Thank you ${user} for registering!`);
        console.log(`%c${user} %cwas registered!%c\n>> %cNew registeredUsers array:`, answerStyleA, answerStyleC, answerStyleA, answerStyleC, registeredUsers);
    }
}
register();
//register("James Jeffries");
//register("suplito");

/*
2. Create a function which will allow us to add a registered user into our friends list.
- this function should be able to receive a string.
- determine if the input username exists in our registeredUsers array.
- if it is, add the foundUser in our friendList array.
-Then show an alert with the following message:
- "You have added <registeredUser> as a friend!"
- if it is not, show an alert window with the following message:
- "User not found."
- invoke the function and add a registered user in your friendsList.
- Outside the function log the friendsList array in the console.

*/
function addFriend(addUser) {
    if (addUser === undefined || addUser === null) {
        return;
    } else if (registeredUsers.includes(addUser)) {
        friendsList.push(addUser);
        alert(`You have added ${addUser} as a friend!`);
        console.log(`You have added %c${addUser} %cas a friend!%c\n>> %cNew friendsList array:`, answerStyleA, answerStyleC, answerStyleA, answerStyleC,  friendsList);
    } else {
        alert(`User ${addUser} not found.`);
        console.log(`User %c${addUser} %cnot found.%c\n>> %cfriendsList array:`, answerStyleA, answerStyleC, answerStyleA, answerStyleC, friendsList);
    }
}
addFriend();
//addFriend("James Jeffries");
//addFriend("Akiko Yukihime");
//addFriend("Gunther Smith");
//addFriend("jd");

/*
3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
- If the friendsList is empty show an alert: 
- "You currently have 0 friends. Add one first."
- Invoke the function.

*/
function displayFriends() {
    
    if (friendsList.length === 0) {
        alert("You currently have 0 friends. Add one first.");
    } else {
        friendsList.forEach((item, index) => console.log(`friend ${index}:\n%c>> `, answerStyleA, item));
    }

}
//displayFriends();


/*
4. Create a function which will display the amount of registered users in your friendsList.
- If the friendsList is empty show an alert:
- "You currently have 0 friends. Add one first."
- If the friendsList is not empty show an alert:
- "You currently have <numberOfFriends> friends."
- Invoke the function

*/
function displayNumberOfFriends() {
    if (friendsList.length === 0) {
        alert("You currently have 0 friends. Add one first.");
    } else {
        alert(`You currently have ${friendsList.length} friends.`)
        console.log(`%c>> %cYou currently have ${friendsList.length} friends.`, answerStyleA, answerStyleC);
    }
}
//displayNumberOfFriends();

/*
5. Create a function which will delete the last registeredUser you have added in the friendsList.
- If the friendsList is empty show an alert:
- "You currently have 0 friends. Add one first."
- Invoke the function.
- Outside the function log the friendsList array.

*/
function deleteLastFriend() {
    if (friendsList.length === 0) {
        alert("You currently have 0 friends. Add one first.");
    } else {
        const removeFriends = friendsList.pop();
        console.log(`%c>> %cdeleted friend: %c${removeFriends} %c\nNew friendsList array:`, answerStyleA, answerStyleC, answerStyleA, answerStyleC, friendsList);
    }
    
}
//deleteLastFriend();

/*
Stretch Goal:

Instead of only deleting the last registered user in the friendsList delete a specific user instead.
-You may get the user's index.
-Then delete the specific user with splice().

*/

function removeFriend(specificUser) {
    const userIndex = friendsList.indexOf(specificUser);

    if (friendsList.length === 0) {
        alert("You currently have 0 friends. Add one first.");
    } else {
        friendsList.splice(userIndex, 1);
        console.log(`%c>> %cremoved friend: %c${specificUser} %c\nNew friendsList array:`, answerStyleA, answerStyleC, answerStyleA, answerStyleC, friendsList);
    }
    
}
//removeFriend("James Jeffries");
//removeFriend("Akiko Yukihime");
//removeFriend("Gunther Smith");



