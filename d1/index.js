console.log("JavaScript - Array Manipulation");

//Array Methods
//JS has built-in functions and methods for arrays
//This allows us to manipulate and access array items

//Mutator Methods
/*
	Mutator Methods are functions that "mutate" or change an array after they are created
	These methods manipulate the original array performing various tasks such as adding and removing elements

*/

let fruits = ['Apple',"Orange", 'Kiwi', 'Dragon Fruit'];

//push
	/*
		adds an element in the end of an array and returns the array length

		Syntax:

		arrayName.push();

	*/
	
	console.log("Current Array: ");
	console.log(fruits);

	let fruitsLength = fruits.push("Mango");
	console.log(fruitsLength);
	console.log("Mutated array from push method: ");
	console.log(fruits);//Mango is added at the end of the array

	fruits.push('Avocado', 'Guava');
	console.log("Mutated array from push method: ");
	console.log(fruits);// Avocado and Guava is added at the end of the array
	//['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado', 'Guava']

//pop

/*
	Removes the last element in an array and returns the removed element
	Syntax:
	
	arrayName.pop();

*/

	let removedFruit = fruits.pop();
	console.log(removedFruit);//Guava
	console.log("Mutated array from pop method: ");
	console.log(fruits);// Guava is removed
	//['Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']




	let ghostFighters = ["Eugene","Dennis","Alfred","Taguro"];

	/*
		Mini Activity 1 (3min)
		Create a function which will remove the last person in the array
		log the ghostFighters array in the console
		Send an ss of your output

	*/

	function removePerson(){

		ghostFighters.pop();
	}

	removePerson();
	console.log(ghostFighters);//['Eugene', 'Dennis', 'Alfred']

	//unshift()

	/*
		adds one or more elements at the beginning of an array

		syntax
			arrayName.unshift(elementA);
			arrayName.unshift(elementA, elementB);
	*/

		fruits.unshift('Lime', 'Banana');
		console.log("Mutated array from unshift method: ");
		console.log(fruits);//['Lime', 'Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']


	//shift()

	/*
		removes an element at the beginning of an array AND returns the removed element

		syntax
			arrayName.shift();

	*/

	let anotherFruit = fruits.shift();
	console.log(anotherFruit);
	console.log("Mutated array from shift method: ");
	console.log(fruits);//['Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']

	//splice
	/*
		simultaneously removes elements from a specified index number and adds the elements

		Syntax
		arrayName.splice(startingIndex,deleteCount,elementsToBeAdded);

	*/

	fruits.splice(1,2,'Lime','Cherry');
	console.log("Mutated array from splice method: ");
	console.log(fruits);//['Banana', 'Lime', 'Cherry', 'Kiwi', 'Dragon Fruit', 'Mango', 'Avocado']

	//sort
	 /*
		Rearranges the array elements in alphanumeric order
		-Syntax
			arrayName.sort();

	 */

	fruits.sort();
	console.log("Mutated array from sort method");
	console.log(fruits);

	//reverse
	 /*
		Reverses the order of array elements
		-Syntax
			arrayName.reverse();
	 */

	fruits.reverse();
	console.log("Mutated array from reverse method");
	console.log(fruits);


	//Non-Mutator Methods
	/*
		Non-Mutator methods are functions that do not modify or change an array after they are created
		-these methods do not manipulate the original array performing various tasks such as returning elements from an array and combining arrays and printing the output
	*/

	let countries = ['US',"PH","CAN",'SG',"TH","PH","FR",'DE'];

	//indexOf()

	/*
		returns the index number of the first matching element found in an array
		--if no match is found, the result is -1
		-the search process will be done from first element processing to the last element

		syntax:
		arrayName.indexOf(searchValue);
		arrayName.indexOf(searchValue,fromIndex)

	*/

		let firstIndex = countries.indexOf('PH');
		console.log("Result of indexOf method: " + firstIndex);//1
		let firstExample = countries.indexOf('ph');
		console.log("Result of indexOf method: " + firstExample);//-1
		let invalidCountry = countries.indexOf('BR');
		console.log("Result of indexOf method: " + invalidCountry);//-1


	//lastIndexOf()

		/*
			returns the index number of the last matching element found in an array
			-the search process will be done from last element processing to the first element

			syntax:
			arrayName.lastIndexOf(searchValue);
			arrayName.lastIndexOf(searchValue,fromIndex)

		*/


		//['US',"PH","CAN",'SG',"TH","PH","FR",'DE'];

		let lastIndex = countries.lastIndexOf('PH', 3);
		console.log("Result of lastIndexOf method: " + lastIndex);//1


	//slice()

		/*
			-portions/slices elements from an array AND returns a new array
			-syntax
				arrayName.slice(startingIndex);
				arrayName.slice(startingIndex,endingIndex);
		*/



		console.log(countries);//['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE']

		let slicedArrayA = countries.slice(2);
		console.log('Result from slice method: ');
		console.log(slicedArrayA);//['CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];
		
		console.log(countries);

		let slicedArrayB = countries.slice(2,4);
		console.log('Result from slice method: ');
		console.log(slicedArrayB);//'CAN', 'SG' //[02,03]

		let slicedArrayC = countries.slice(-3);
		console.log('Result from slice method: ');
		console.log(slicedArrayC);//['PH', 'FR', 'DE']


	//toString()

		/*
			retruns an array as a string separated by commas
			Syntax:
			arrayName.toString();

		*/

		let stringArray = ghostFighters.toString();
		console.log("Result from toString method: ");
		console.log(stringArray);//Eugene,Dennis,Alfred
		console.log(typeof stringArray);//string

	//concat()
		/*
			combine two arrays and returns the combined result
			-Syntax
				arrayA.concat(arrayB);
				arrayA.concat(elementA);
		*/


		let taskArrayA = ['drink html', 'eat JavaScript'];
		let taskArrayB = ['inhale css', 'breathe react'];
		let taskArrayC = ['get git', 'be node'];

		let tasks = taskArrayA.concat(taskArrayB);
		console.log('Result from concat method: ');
		console.log(tasks);//['drink html', 'eat JavaScript', 'inhale css', 'breathe react']

		//combine multiple arrays

		console.log("Result from concat method: ");
		let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
		console.log(allTasks);//['drink html', 'eat JavaScript', 'inhale css', 'breathe react', 'get git', 'be node']

		//combine arrays with elements
		let combinedTasks = taskArrayA.concat('smell express', 'throw mongodb');
		console.log("Result from concat method: ");
		console.log(combinedTasks);//['drink html', 'eat JavaScript', 'smell express', 'throw mongodb']

	//join
		/*
			Returns an array as a string separated by specified separator
			-syntax

			arrayName.join('separatorString');
		*/

		let users = ["John","Jane","Joe","Robert","Neil"];

		console.log(users.join());//John,Jane,Joe,Robert,Neil
		console.log(users.join(' '));
		console.log(users.join(' - '));
		console.log(users.join(' * '));//John * Jane * Joe * Robert * Neil

	//Iteration Method

	/*
		Iteration methods are loops designed to perform repetitive tasks on arrays
		Iteration methods loops over all items in an array
		Useful for manipulating array data resulting in complex tasks
	*/

//forEach
	/*
		similar to for loop that iterates on each array element
		-for each item in the array, the anonymous function passed in the forEach() method will be run
		-anonymous function is able to receive the current item being iterated or loop over by assigning a parameter
		-variable names for arrays are normally written in the plural form of the data stored in an array
		-it's common practice to use the singular form of the array content for parameter names used in array loops
		-forEach() does NOT return anything

		Syntax
		arrayName.forEach(function(indivElement){
			statement
		})


	*/
		//['drink html', 'eat JavaScript', 'inhale css', 'breathe react', 'get git', 'be node']

		allTasks.forEach(function(task){
			console.log(task);
		});

	/*
		Mini Activity #2
		create a function that can display the ghostFighters one by one in our console
		Invoke the function
		send a screenshot
	*/
		function displayGhostFighters(){
			ghostFighters.forEach(function(fighter){
				console.log(fighter);
			});
		}
		displayGhostFighters();


		//use forEach with conditional statements and also push

		/*
			It is a good practice to print the current elemnt in the console when working with array iteration methods to have an idea of what information is being worked on for each iteration of the loop

			Creatng a sperate variable to store results of an array iteration iteration methods are also good practice to avoid confustion by modifying the original array

			Mastering loops and arrays allow us developers to perform a wide range of features that help in data management and analysis

		*/
		

		let filteredTasks = [];

		allTasks.forEach(function(task){
			//console.log(task)


			//we "filtered out" tasks with greater than 10 chracters 
			if(task.length>10){
				console.log(task + "my length is greater than 10");
				//we added/pushed the "tasks" filtered into the filteredTasks array
				//it is no longer an empty array
				filteredTasks.push(task)
			}


		});

		console.log("Result of filteredTasks: ");
		console.log(filteredTasks); //before [] //after ['eat JavaScript', 'breathe react']

		//map()

		/*
			iterates on each element and returns NEW ARRAY with different VALUES depending on the RESULT of the function's operation
			
			-syntax
				let/const resultArray = arrayName.map(function(indivElement))


		*/

		let numbers = [1,2,3,4,5];
		let numberMap = numbers.map(function(number){
			return number * number;
		})

		console.log("Original Array: ");
		console.log(numbers);//[1,2,3,4,5]
		console.log("Result of map method: ");
		console.log(numberMap);//[1,4,9,16,25]

		//map() vs forEach()

		let numberForEach = numbers.forEach(function(number){
			return number * number;
		})

		console.log(numberForEach); //undefined

		//every()
		/*
			-checks if all elements in an array meet the given conditions
			-this is useful for validating data storesd in arrays especially when dealing with large amounts of data
			-returns true if all elements meet the condition, otherwise false
			-Syntax
			let/const resultArray = arrayName.every(function(indivElement){
				return expression/condition;
			})
			*/
		//[1,2,3,4,5];

		let allValid = numbers.every(function(number){
			return (number < 3)
		});
		console.log(allValid);//false

		//some()
		/*
			Checks if at least one element in the array meets the given condition
			-returns a true value if at least one element meets the condition and false if otherwise
			Syntax
				let/const resultArray = arrayName.some(function(indivElement){
				return expression/condition;
				})
		*/

		let someValid = numbers.some(function(number){
			return (number < 2)
		});
		console.log(someValid);//true

		//Combining the returned result from every/some method may be used in other statements to perform consecutive results
		if(someValid){
			console.log('Some numbers in the array are greater than 2');
		}


		//filter()
		/*
			returns a new array that contains elements which meets a given condition
			-returns an empty array if no elements were found
			syntax:

			let/const resultArray = arrayName.filter(function(indivElemet){
					return expression/condition;
			})

		*/s

		let filterValid = numbers.filter(function(number){
			return(number < 3)
		})
		console.log(filterValid);//[1,2]

		//[1,2,3,4,5];

		let nothingFound = numbers.filter(function(number){
			return (number=0)
		})
		console.log(nothingFound);//[]

		//filtering using forEach
		let filteredNumbers = [];

		numbers.forEach(function(number){

			if(number<3){
				filteredNumbers.push(number);
			}

		})
		console.log(filteredNumbers);//[1,2]

		//includes()

		/*
			checks if the argument passed can be found in the array
				-it returns a boolean which can be saved in a variable
					-returns true if the argument is found the array
					-returns false if it is not

				Syntax:
				arrayName.includes(<argumentToFind>)

		*/

		let products = ["Mouse","Keyboard","Laptop","Monitor"];

		let productFound = products.includes("Mouse");
		console.log(productFound);//case sensitive//true

		let productNotFound = products.includes("Headset");
		console.log(productNotFound);//false

		//Method chaining
		//Methods can be chained using them one after another
		//The result of the first method is used on the second method until all "chained" methods have been resolved

		let filteredProducts = products.filter(function(product){
			return product.toLowerCase().includes('a');
		})

		console.log(filteredProducts);//['Keyboard', 'Laptop']


	/*
		Mini Activity #3
		Create an addTrainer function that will enable us to add a trainer in the contacts array
		--This function should be able to receive a string
		--Determine if the added trainer already exists in the contacts array:
		--if it is, show an alert saying "Already added in the Match Call(Contacts)"
		--if it is not, add the trainer in the contacts array and show an alert saying "Registered!"
		--invoke and add a trainer in the browser's console
		--in the console, log the contacts array
	*/

		let contacts = ["Ash"];

		function addTrainer(trainer){

			let doesTrainerExist = contacts.includes(trainer);

			if(doesTrainerExist){
				alert("Already added in the Match Call.");
			}else{
				contacts.push(trainer);
				alert("Registered!");
			}

		}
		//addTrainer("Misty");
		//console.log(contacts);



//reduce

		/*

			-evaluates elements from left to right and returns/reduces the array into a single value
			-Syntax
			let/const resultArray = arrayName.reduce(function(accumulator,currenValue){
				return expression/operation
			})
				-"accumulator" parameter in the function stores the result for every iteration of the loop
				="currentValue" is the current/next element in the array that is evaluated in each iteration of the loop
			
			How the "reduce" method works
			1. the first/result element in the array is stored in the "accumulator" parameter
			2/ the second/next element in the array is stored in the currentvalue parameter
			3. an operation is performed ont he two elemts
			4. the loop repates step 1-3 until all elements have been worked on
		*/

	console.log(numbers);//[1, 2, 3, 4, 5]

	let iteration = 0;

	let reducedArray = numbers.reduce(function(x,y){

		console.warn("Current iteration: " + ++iteration);
		console.log('accumulator: ' + x);
		console.log("current value: " + y);

		return x + y;

	})

	console.log("Result of reduce method: " + reducedArray);

	let iterationStr = 0;

	let list = ["Hello", "Again", "World"];

	let reducedJoin = list.reduce(function(x,y){

		console.warn("Current iteration: " + ++iterationStr);
		console.log('accumulator: ' + x);
		console.log("current value: " + y);

		return x + ' ' + y;

	})

	console.log(reducedJoin);